import logger from "./logger.js";

module.exports = {
    database: "eapi",
    username: "",
    password: "",
    params: {
        dialect: "sqlite",
        storage: "eapi.sqlite",
        logging: (sql) => {
            logger.info(`[${new Date()}] ${sql}`);
        },
        define: {
            underscored: true
        }
    },
    jwtSecret: "Ncj1y>]1",
    jwtSession: {session: false}
};