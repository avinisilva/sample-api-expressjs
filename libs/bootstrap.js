import https from "https";
import fs from "fs";

module.exports = app => {
    if (process.env.NODE_ENV !== "test") {
        const credentials = {
            key: fs.readFileSync("23039528-eapi.key", "utf8"),
            cert: fs.readFileSync("23039528-eapi.cert", "utf8")
        };

        app.database.sequelize.sync().done(() => {
            https.createServer(credentials, app)
                .listen(app.get("port"), () => {
                    console.log(`API - port  ${app.get("port")}`);
                });
        });
    }
};