module.exports = {
    database: "eapi_test",
    username: "",
    password: "",
    params: {
        dialect: "sqlite",
        storage: "eapi.sqlite",
        logging: false,
        define: {
            underscored: true
        }
    },
    jwtSecret: "Ncj1y>]1",
    jwtSession: {session: false}
};