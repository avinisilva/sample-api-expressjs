import jwt from "jwt-simple";

describe("Routes: Tasks", () => {
    const Users = app.database.models.Users;
    const jwtSecret = app.libs.config.jwtSecret;
    let token;

    beforeEach(done => {
    });
    describe("GET /user", () => {
        describe("status 200", () => {
            it("returns an authenticated user", done => {
            });
        });
    });
    describe("DELETE /user", () => {
        describe("status 200", () => {
            it("deletes an authenticated user", done => {
            });
        });
    });
    describe("POST /users", () => {
        describe("status 200", () => {
            it("creates a new user", done => {
            });
        });
    });
});