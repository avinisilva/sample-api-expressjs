import express from  "express";
import consign from "consign";

const app = express();

consign({verbose: false})
    .include("libs/config.js")
    .then("database.js")
    .then("authorization.js")
    .then("libs/middlewares.js")
    .then("routes")
    .then("libs/bootstrap.js")
    .into(app);


// exports for tests
module.exports = app;


