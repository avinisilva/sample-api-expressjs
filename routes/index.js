module.exports = app => {

    /**
     * @api {get} / API Status
     * @apiGroup Status
     * @apiSuccess {String} API status message
     * @apiSuccessExample {json} Sucesso
     *
     HTTP/1.1 200 OK
     *
     {"status": "ExpressAPI EApi"}
     */
    app.get('/', (req, res) => {
        res.json({status: "ExpressAPI EApi"});
    });
};