module.exports = app => {
    const Users = app.database.models.Users;

    app.route("/user")
        .all(app.authorization.authenticate())
        /**
         * @api {get} /user Shows authenticated user
         * @apiGroup User
         * @apiHeader {String} Authorization token of user
         * @apiHeaderExample {json} Header
         *
         {"Authorization": "JWT xyz.abc.123.hgf"}
         * @apiSuccess {Number} id Id of record
         * @apiSuccess {String} name Name
         * @apiSuccess {String} email Email
         * @apiSuccessExample {json} Success
         *
         HTTP/1.1 200 OK
         *
         {
         *
         "id": 1,
         *
         "name": "John Connor",
         *
         "email": "john@connor.net"
         *
         }
         * @apiErrorExample {json} Error in the query
         *
         HTTP/1.1 412 Precondition Failed
         */
        .get((req, res) => {
            Users.findById(req.user.id, {
                attributes: ["id", "name", "email"]
            })
                .then(result => res.json(result))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        })
        /**
         *
         *
         *
         *
         *
         *
         @api {delete} /user Delete user authenticated
         @apiGroup User
         @apiHeader {String} Authorization token of user
         @apiHeaderExample {json} Header
         {"Authorization": "JWT xyz.abc.123.hgf"}
         @apiSuccessExample {json} Success
         *
         HTTP/1.1 204 No Content
         * @apiErrorExample {json} Error in delete
         *
         HTTP/1.1 412 Precondition Failed
         */
        .delete((req, res) => {
            Users.destroy({where: {id: req.user.id} })
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        });

        /**
        * @api {post} /users New user
        * @apiGroup User
        * @apiParam {String} name Name
        * @apiParam {String} email Email
        * @apiParam {String} password Password
        * @apiParamExample {json} Input
        *    {
        *      "name": "John Connor",
        *      "email": "john@connor.net",
        *      "password": "123456"
        *    }
        * @apiSuccess {Number} id Id of record
        * @apiSuccess {String} name Name
        * @apiSuccess {String} email Email
        * @apiSuccess {String} password Password encrypted
        * @apiSuccess {Date} updated_at Update date
        * @apiSuccess {Date} created_at Create date
        * @apiSuccessExample {json} Success
        *    HTTP/1.1 200 OK
        *    {
        *      "id": 1,
        *      "name": "John Connor",
        *      "email": "john@connor.net",
        *      "password": "$2a$10$SK1B1",
        *      "updated_at": "2015-09-24T15:46:51.778Z",
        *      "created_at": "2015-09-24T15:46:51.778Z"
        *    }
        * @apiErrorExample {json} Error in the register
        *    HTTP/1.1 412 Precondition Failed
        */
        app.post("/users", (req, res) => {
            Users.create(req.body)
                 .then(result => res.json(result))
                 .catch(error => {
                     res.status(412).json({msg: error.message});
                 });
        });
 };