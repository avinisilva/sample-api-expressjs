module.exports = app => {
    const Tasks = app.database.models.Tasks;

    app.route("/tasks")
        .all(app.authorization.authenticate())
        /**
         * @api {get} /tasks Tasks list
         * @apiGroup Tasks
         * @apiHeader {String} Authorization Token of user
         * @apiHeaderExample {json} Header
         *    {"Authorization": "JWT xyz.abc.123.hgf"}
         * @apiSuccess {Object[]} tasks Tasks list
         * @apiSuccess {Number} tasks.id Id of record
         * @apiSuccess {String} tasks.title Title of task
         * @apiSuccess {Boolean} tasks.done Task was completed?
         * @apiSuccess {Date} tasks.updated_at Update date
         * @apiSuccess {Date} tasks.created_at Create date
         * @apiSuccess {Number} tasks.user_id Id of user
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    [{
         *      "id": 1,
         *      "title": "To study",
         *      "done": false
         *      "updated_at": "2015-09-24T15:46:51.778Z",
         *      "created_at": "2015-09-24T15:46:51.778Z",
         *      "user_id": 1
         *    }]
         * @apiErrorExample {json} Error in the query
         *    HTTP/1.1 412 Precondition Failed
         */
        .get((req, res) => {
            Tasks.findAll({where: { user_id: req.user.id }})
                .then(result => { res.json(result) })
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        })
        /**
         * @api {post} /tasks New Task
         * @apiGroup Tasks
         * @apiHeader {String} Authorization Token of user
         * @apiHeaderExample {json} Header
         *    {"Authorization": "JWT xyz.abc.123.hgf"}
         * @apiParam {String} title Title task
         * @apiParamExample {json} Input
         *    {"title": "To study"}
         * @apiSuccess {Number} id Id of record
         * @apiSuccess {String} title Title task
         * @apiSuccess {Boolean} done=false Task was completed?
         * @apiSuccess {Date} updated_at Update date
         * @apiSuccess {Date} created_at Create date
         * @apiSuccess {Number} user_id Id of user
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    {
         *      "id": 1,
         *      "title": "To study",
         *      "done": false,
         *      "updated_at": "2015-09-24T15:46:51.778Z",
         *      "created_at": "2015-09-24T15:46:51.778Z",
         *      "user_id": 1
         *    }
         * @apiErrorExample {json} Error in the query
         *    HTTP/1.1 412 Precondition Failed
         */

        .post((req, res) => {
            req.body.user_id = req.user.id;
            Tasks.create(req.body)
                .then(result => res.json(result))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        });

    app.route("/tasks/:id")
        .all(app.authorization.authenticate())
        /**
         * @api {get} /tasks/:id Show Task
         * @apiGroup Tasks
         * @apiHeader {String} Authorization Token of user
         * @apiHeaderExample {json} Header
         *    {"Authorization": "JWT xyz.abc.123.hgf"}
         * @apiParam {id} id Id task
         * @apiSuccess {Number} id Id of record
         * @apiSuccess {String} title Title task
         * @apiSuccess {Boolean} done Task was completed?
         * @apiSuccess {Date} updated_at Update date
         * @apiSuccess {Date} created_at Create date
         * @apiSuccess {Number} user_id Id of user
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    {
         *      "id": 1,
         *      "title": "To study",
         *      "done": false
         *      "updated_at": "2015-09-24T15:46:51.778Z",
         *      "created_at": "2015-09-24T15:46:51.778Z",
         *      "user_id": 1
         *    }
         * @apiErrorExample {json} Task does not exist
         *    HTTP/1.1 404 Not Found
         * @apiErrorExample {json} Error in the query
         *    HTTP/1.1 412 Precondition Failed
         */
        .get((req, res) => {
            Tasks.findOne({where: {id: req.params.id, user_id: req.user.id}})
                .then(result => {
                    if (result) {
                        res.json(result);
                    } else {
                        res.sendStatus(404);
                    }
                })
                .catch(error => {
                    res.status(412).json({msg: error.message});
                })
        })
        /**
         * @api {put} /tasks/:id Update Task
         * @apiGroup Tasks
         * @apiHeader {String} Authorization Token of user
         * @apiHeaderExample {json} Header
         *    {"Authorization": "JWT xyz.abc.123.hgf"}
         * @apiParam {id} id Id task
         * @apiParam {String} title Title task
         * @apiParam {Boolean} done Task was completed?
         * @apiParamExample {json} Input
         *    {
         *      "title": "Work",
         *      "done": true
         *    }
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 204 No Content
         * @apiErrorExample {json} Error in the query
         *    HTTP/1.1 412 Precondition Failed
         */
        .put((req, res) => {
            Tasks.update(req.body, {where: {id: req.params.id, user_id: req.user.id}})
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        })
        /**
         * @api {delete} /tasks/:id Remove Task
         * @apiGroup Tasks
         * @apiHeader {String} Authorization Token of user
         * @apiHeaderExample {json} Header
         *    {"Authorization": "JWT xyz.abc.123.hgf"}
         * @apiParam {id} id Id task
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 204 No Content
         * @apiErrorExample {json} Error in the query
         *    HTTP/1.1 412 Precondition Failed
         */
        .delete((req, res) => {
            Tasks.destroy({where: {id: req.params.id, user_id: req.user.id}})
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        });

};
