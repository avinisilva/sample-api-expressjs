import jwt from "jwt-simple";

module.exports = app => {
    const cfg = app.libs.config;
    const Users = app.database.models.Users;

    /**
     * @api {post} /token Token authenticated
     * @apiGroup Credential
     * @apiParam {String} email Email of user
     * @apiParam {String} password Password of user
     * @apiParamExample {json} Input
     *
     {
     *
     "email": "john@connor.net",
     *
     "password": "123456"
     *
     }
     * @apiSuccess {String} token Token of user authenticated
     * @apiSuccessExample {json} Success
     *
     HTTP/1.1 200 OK
     *
     {"token": "xyz.abc.123.hgf"}
     * @apiErrorExample {json} Error of authentication
     *
     HTTP/1.1 401 Unauthorized
     */
    app.post("/token", (req, res) => {
        if (req.body.email && req.body.password) {
            const email = req.body.email;
            const password = req.body.password;
            Users.findOne({where: {email: email}})
                .then(user => {
                    if (Users.isPassword(user.password, password)) {
                        const payload = {id: user.id};
                        res.json({
                            token: jwt.encode(payload, cfg.jwtSecret)
                        });
                    } else {
                        res.sendStatus(401);
                    }
                })
                .catch(error => res.sendStatus(401));
        } else {
            res.sendStatus(401);
        }
    });
};