## API NodeJS with ES6 and Babel

#### Usage

```
$ git clone https://github.com/seusoft/eapi.git
$ cd eapi
$ npm install

$ npm run clusters
```

#### Test

```
$ npm test
```

#### Framework
- Express 4.x

